package ru.t1.nikitushkina.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.service.PropertyService;
import ru.t1.nikitushkina.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "user1";

    @NotNull
    public final static String USER_TEST_PASSWORD = "user1pwd";

    @NotNull
    public final static String USER_TEST_EMAIL = "user1@a.ru";

    @NotNull
    public final static String USER2_TEST_LOGIN = "user2";

    @NotNull
    public final static String USER2_TEST_PASSWORD = "user2pwd";

    @NotNull
    public final static String USER2_TEST_EMAIL = "user2@a.ru";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "user3";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "user3pwd";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "user3@a.ru";

    @NotNull
    public final static User USER_TEST = new User();

    @NotNull
    public final static User USER2_TEST = new User();

    @NotNull
    public final static User ADMIN_TEST = new User();

    @Nullable
    public final static User NULL_USER = null;

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER_TEST, USER2_TEST, ADMIN_TEST);

    @NotNull
    public final static List<User> USER_LIST_ADDED = Arrays.asList(ADMIN_TEST);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(propertyService, USER_TEST_PASSWORD));
        USER_TEST.setEmail(USER_TEST_EMAIL);

        USER2_TEST.setLogin(USER2_TEST_LOGIN);
        USER2_TEST.setPasswordHash(HashUtil.salt(propertyService, USER2_TEST_PASSWORD));
        USER2_TEST.setEmail(USER2_TEST_EMAIL);

        ADMIN_TEST.setLogin(ADMIN_TEST_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD));
        ADMIN_TEST.setEmail(ADMIN_TEST_EMAIL);
    }

}
